import java.util.Scanner;

/**
 * Created by Farmer on 2018-02-17.
 */
class UI {
    private Users users;
    void startingPanel(){
        Tools.fillUsersDatabase();
        users = Users.getInstance();

        Person person = new Person(Tools.getStringFromUser("Podaj imię:"), Tools.getStringFromUser("Podaj nazwisko:"));

        if(users.isPersonExist(person)){
            while(true) {
                String password = Tools.getStringFromUser("Podaj hasło");
                if (users.isPasswordCorrect(person, password)) {
                    mainPanel();
                    break;
                } else {
                    System.out.println("niepoprawne hasło");
                }
            }
        } else{
            System.out.println("Osoby nie ma w rejestrze");
        }
    }

    private void mainPanel() {
        Scanner scanner = new Scanner(System.in);

        i : while(true) {
            System.out.println("\nWybierz co chcesz zrobić:" +
                    "\n1.Wyświetl użytkowników" +
                    "\n2.Wyświetl kurs waluty z wybranego dnia" +
                    "\n3.Wyświetl ostatnie kursy wybranej waluty" +
                    "\n4.Wyświetlanie wybranej waluty z wybranego przedziału dat" +
                    "\n5.Dodaj użytkownia" +
                    "\n6.Wyloguj się\n");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1: {
                    System.out.println(users);
                    break;
                }
                case 2: {

                    Data.printRatesFromOneDay(Tools.getStringFromUser("Podaj walutę:"), Tools.getStringFromUser("Podaj datę (YYYY-MM-DD):"));

                    break;
                }
                case 3: {

                    Data.printRatesOfSelectedCurrency(Tools.getStringFromUser("Podaj walutę:")
                            , Tools.getStringFromUser("Podaj, ile ostatnich kursów wyświetlić:"));

                    break;
                }
                case 4: {

                    Data.printRatesOfSelectedCurrencyBetweenDates(Tools.getStringFromUser("Podaj walutę:"),
                            Tools.getStringFromUser("Podaj datę początkową (YYYY-MM-DD)"),
                            Tools.getStringFromUser("Podaj datę końcową (YYYY-MM-DD)"));

                    break;
                }
                case 5: {
                    while(true) {
                        if (PasswordChecker.isAdministratorPasswordCorrect(Tools.getStringFromUser("Podaj hasło administratora:"))) {
                            Person person = new Person(Tools.getStringFromUser("Podaj imię nowego użytkownika:"),
                                    Tools.getStringFromUser("Podaj nazwisko nowego użytkownika:"));
                            String password = Tools.getStringFromUser("Podaj hasło nowego użytkownika:");
                            users.addUser(person, password);
                            Tools.saveUserToFile(person,password);
                            break;
                        }else{
                            System.out.println("Hasło użytkownia nie poprawne, spróbuj ponownie");
                        }
                    }
                    break;
                }
                case 6: {
                    break i;
                }
                default: {
                    System.out.println("błąd wprowadzonych danych. spróbuj ponownie");
                }
            }
        }
    }
}
