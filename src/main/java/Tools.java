import java.io.*;
import java.util.Scanner;
import java.util.function.Predicate;

/**
 * Created by Beata1 on 2018-02-17.
 */
public class Tools {

    static String getStringFromUser(String message){
        System.out.println(message);
        Scanner scanner = new Scanner(System.in);
        String result = scanner.nextLine();
        return result;
    }

    static void fillUsersDatabase(){
        Users users = Users.getInstance();
        String line;
        try {
            FileReader fileReader = new FileReader("users.txt");
            BufferedReader br = new BufferedReader(fileReader);
            line = br.readLine();
            while(line != null){
                String[] user;
                user = line.split(",");
                String name = user[0];
                String surname = user[1];
                String password = user[2];

                users.addUser(new Person(name,surname),password);

                line = br.readLine();
            }
        } catch (FileNotFoundException e){

        }catch (IOException e){
        }
    }

    static void saveUserToFile(Person person, String password){
        try {
            FileWriter fileWriter = new FileWriter("users.txt",true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(bufferedWriter);
            printWriter.print("\n");
            printWriter.print(person.getImie() + "," + person.getNazwisko() + "," + password);
            printWriter.close();

        }catch (IOException e){

        }
    }
}
