/**
 * Created by Farmer on 2018-02-17.
 */
public class Person {
    private String imie;
    private String nazwisko;

    Person(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    String getImie() {
        return imie;
    }

    void setImie(String imie) {
        this.imie = imie;
    }

    String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Person{");
        sb.append("imie='").append(imie).append('\'');
        sb.append(", nazwisko='").append(nazwisko).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
