import java.util.HashMap;
import java.util.Map;

/**
 * Created by Farmer on 2018-02-17.
 */
public class Users {
    private static Users instance;
    private Map<Person, String> mapOfUsers;

    private Users() {
        mapOfUsers = new HashMap<Person, String>(){};
    }

    public static Users getInstance(){
        if(instance == null){
            instance = new Users();
        }
        return instance;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("");
        for (Person person : mapOfUsers.keySet()){
            sb.append("Imię: " + person.getImie());
            sb.append("\t");
            sb.append("Nazwisko: " + person.getNazwisko());

            sb.append("\n");
        }
        return sb.toString();
    }

    void addUser(Person person, String password){
        if(!PasswordChecker.isCorrectLength(password)){
            System.out.println("Hasło musi mieć conajmniej 6 znaków");
            return;
        }
        if(mapOfUsers.containsKey(person)){
            System.out.println("Użytkownik już istnieje");
            return;
        }
        mapOfUsers.put(person,password);
    }

    boolean isPersonExist(Person person) {
        for(Person p : getMapOfUsers().keySet()){
            if(p.getImie().equals(person.getImie()) && p.getNazwisko().equals(person.getNazwisko())){
                return true;
            }
        }
        return false;
    }

    boolean isPasswordCorrect(Person person, String password){
        for(Person p : getMapOfUsers().keySet()){
            if(p.getImie().equals(person.getImie()) && p.getNazwisko().equals(person.getNazwisko()) && getMapOfUsers().get(p).equals(password) ){
                return true;
            }
        }
        return false;
    }

    Map<Person, String> getMapOfUsers() {
        return mapOfUsers;
    }


}
