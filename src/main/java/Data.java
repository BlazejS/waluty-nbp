import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;


/**
 * Created by Beata1 on 2018-02-17.
 */
class Data {

    static void printRatesFromOneDay(String currency, String date) {
        try {
            JSONObject jsonObjectWithOneDay = getDataFromURL("http://api.nbp.pl/api/exchangerates/rates/a/" + currency + "/"
                    + date + "/?format=json");
            System.out.println(date + ": Kurs: " + jsonObjectWithOneDay.getJSONArray("rates").getJSONObject(0).get("mid"));
        } catch (Exception e) {
            System.out.println("Brak danych.");
        }
    }

    static void printRatesOfSelectedCurrency(String currency, String amount) {
        try {
            JSONObject jsonObject = getDataFromURL("http://api.nbp.pl/api/exchangerates/rates/a/" + currency + "/last/" + amount + "/?format=json");
            JSONArray jsonArray = jsonObject.getJSONArray("rates");
            System.out.println("===KURSY " + currency.toUpperCase() + " WG DAT===");
            for (int i = 0; i < jsonArray.length(); i++) {
                System.out.println(jsonArray.getJSONObject(i).getString("effectiveDate")
                        + ": Kurs: " + jsonArray.getJSONObject(i).getDouble("mid"));
            }
        } catch (Exception e) {
            System.out.println("Brak danych");
        }
    }

    static void printRatesOfSelectedCurrencyBetweenDates(String currency, String startDate, String endDate) {
        try {
            JSONObject jsonObject = getDataFromURL("http://api.nbp.pl/api/exchangerates/rates/a/"
                    + currency + "/" + startDate + "/" + endDate + "/?format=json");

            JSONArray jsonArray = jsonObject.getJSONArray("rates");
            System.out.println("===KURSY " + currency.toUpperCase() + " WG DAT (OD " + startDate + " DO " + endDate + ")===");
            for (int i = 0; i < jsonArray.length(); i++) {
                System.out.println(jsonArray.getJSONObject(i).getString("effectiveDate")
                        + ": Kurs: " + jsonArray.getJSONObject(i).getDouble("mid"));
            }
        } catch (Exception e) {
            System.out.println("Brak danych");
        }
    }


    private static JSONObject getDataFromURL(String url) {
        try {
            String result = "";
            URL nbp = new URL(url);
            BufferedReader in = new BufferedReader(new InputStreamReader(nbp.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                result += inputLine;
            in.close();
            return new JSONObject(result);
        } catch (Exception e) {
            System.out.println("Problem z importem bazy danych.");
            return null;
        }

    }
}
