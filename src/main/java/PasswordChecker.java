/**
 * Created by Farmer on 2018-02-17.
 */
 class PasswordChecker {
    private static String administratorPassword = "123456";

    private static String getAdministratorPassword() {
        return administratorPassword;
    }

    static boolean isAdministratorPasswordCorrect(String password){
        return password.equals(getAdministratorPassword()) ? true : false;
    }
    static boolean isCorrectLength(String password){
        return password.length() > 5 ? true : false;
    }
}
